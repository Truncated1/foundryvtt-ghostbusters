# West End Games GB System
An unauthorized Foundry VTT system for playing a certain old D6 West End game system involving ghosts.

# Installation
In the Foundry VTT setup screen/Install System use the following URL:
```
https://gitlab.com/api/v4/projects/27321737/packages/generic/gb/0.0.0/system.json
```

0.0.0 will always point to the latest release.
To install a specific version replace 0.0.0 with the version you would like.

# Usage
It's really not much more than a character sheet with rollable buttons.
There is little/no automation, although the roll dialogs do provide input entries for Brownie Points, bonuses and penalties.
Brownie points spent on rolls this way will deduct from the character's brownie points.
There's a few dropdowns for damage and encumbrance effects.
Those do not modify the character sheet; they merely display the effect text, and it's up to the player to modify their traits.

All rolls get calculated from the "Current" value of the trait.
If there are any bonus or penalty dice for a roll they can be included in the roll dialog.

# Gear
Gear will have to be created by the GM as items in the sidebar, or players can also hit the "+" button to manually add/edit items.
There isn't any rollable items support yet, maybe later.
(Things like bonuses for a machine gun).

# Traits/Power/Special Abilities
The little gear icon in the upper right of the character sheet allows you to activate/deactivate individual traits.
If you are making an ectoplasmic entity, for example, you can disable the unneeded traits on the entity actor sheet.

# Ghost Die
The Ghost Die will roll 0-5, with the Ghost popping up a roll of 0.
If you roll a Ghost, you'll see "GHOST DIE!" in red on the chat message.

If you have Dice So Nice installed (https://foundryvtt.com/packages/dice-so-nice/) a custom die preset can be used.
This will have the Ghost Die appear with a custom face when the ghost is rolled.
If you wish to use a different image, a 256x256 png, webp, or jpg file can be set in System Settings.

# Macros
A basic compendium of macros has been included.
The macros are for rolling traits and talents, including Power.

# The Future
* Fixing up the ghost die
* Making items rollable
* Possibly making special abilities items rather than just a list of text
* Making opposable rolls against NPCs automated
* Adding a difficulty drop-down to the roll dialog

# Credits
The Boilerplate System (https://gitlab.com/asacolips-projects/foundry-mods/boilerplate), created by asacolips, is the base.
game-icons.net for the Ghost image used on the Ghost Die.
Lots of help from the Foundry VTT and League of Extraordinary Developers Discord channels.
